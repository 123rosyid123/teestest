from app import UserProfile, db
from datetime import datetime
from werkzeug.security import generate_password_hash
import uuid

db.create_all()
hashed_password = generate_password_hash('admin', method='sha256')

new_user = UserProfile(publik_id=str(uuid.uuid4()), username='admin', password=hashed_password, is_admin=True)
db.session.add(new_user)
db.session.commit()