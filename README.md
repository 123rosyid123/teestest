# TeesTest

Simple Rest API dibuat Menggunakan Python Framework Flask, dengan authentikasi JWT 

****Brief****
>  *  JWT
>  *  List, menampilkan data dalam format JSON dengan field Nama, Email, dan Ukuran Baju
>  *  CRUD dari table data di atas
>  *  CRUD untuk user profile, minimal mencakup penggantian profile picture
>  *  TDD/BDD
>  *  Menggunakan container menjadi nilai tambah

****Available URL****

1.  `localhost:5000/login` => GET
2.  `localhost:5000/user` => GET,POST
3.  `localhost:5000/user/<id>` => GET,PUT,DELETE
4.  `localhost:5000/customer` => GET,POST
5.  `localhost:5000/customer/<id>` => GET,PUT,DELETE

**Setup Database Configuration**

Pada app.py gunakan settingan database sesuai dengan kebutuhan yang dijalankan
> ![db_config](https://i.ibb.co/6ZkCPSP/db-config.png)

****Step by step menjalankan aplikasi****
*  pastikan dipc sudah terinstall python3, jika belum maka install dahulu
*  Buat virtual environment
  `python3 -m venv nama-env`
*  Jalankan environment
  `source nama-env/bin/activate`
*  Install requirements yang dibutuhkan
  `pip install -r requirements.txt`
*  Jalankan inisialisasi awal (hanya perlu sekali untuk menggenerate database)
  `python initialize.py`
*  export app flask yang akan dijalankan
  `export FLASK_APP=app.py`
*  Jalankan server
  `flask run` atau `python app.py`
*  Gunakan aplikasi postman atau sejenisnya


****Jika ingin menjalankan automatic test****

Script: `python test.py`

****Docker****

setup docker: `docker-compose up`


****Rule****
*  Untuk mengakses harus menggunakan token
*  Token didapatkan dari response yang diberikan saat login, token hanya berlaku 30 menit
*  User yang berstatus admin dapat menambah, mengubah, dan menghapus user lainnya
*  User yang bukan admin hanya dapat mengubah data diri pribadi
*  Customer dibuat berdasarkan user
*  Setiap user tidak dapat melihat customer user lainnya


****HOW to****
1.  Login and get token `localhost:5000/login`
    
    default `username : admin, password :admin`
> ![login](https://i.imgur.com/inDJrT2.gif)
2.  Tampilkan data user dan Customer `localhost:5000/user`,`localhost:5000/user/<id>`, `localhost:5000/customer`, `localhost:5000/customer/<id>` method `GET`
> ![show](https://i.imgur.com/J7vfMqN.gif)
3.  Create User (hanya berlaku jika user adalah admin) `localhost:5000/user` `POST`
    *  data = username, password, email, nama_lengkap, alamat, no_hp, photo 
> ![create user](https://i.imgur.com/wMQ7rPp.gif)
4.  Update User `localhost:5000/user/<id>` `PUT`
    *  admin = username, password, email, nama_lengkap, alamat, no_hp, photo, is_admin
    *  nonadmin = username, password, email, nama_lengkap, alamat, no_hp, photo
> ![edit user](https://i.imgur.com/MeSDyKx.gif)
5.  Delete User (hanya berlaku jika user adalah admin) `localhost:5000/user/<id>` `DELETE`
> ![delete user](https://i.imgur.com/eb8uzSo.gif)
6.  Create Customer `localhost:5000/customer` `POST`
    *  data = nama, email, ukuran_baju
> ![create customer](https://i.imgur.com/MPo8K2P.gif)
7.  Update Customer `localhost:5000/customer/<id>` `PUT`
> ![update customer](https://i.imgur.com/ccChToO.gif)
8.  Delete User `localhost:5000/customer/<id>` `DELETE`
> ![delete customer](https://i.imgur.com/2dgejBF.gif)
