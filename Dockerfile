FROM python:3.6

Run pip install --upgrade pip
EXPOSE 5000

WORKDIR /app

COPY . /app

RUN pip --no-cache-dir install -r requirements.txt

CMD python app.py

