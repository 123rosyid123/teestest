from app import app, UserProfile, Customer, db
import unittest
from base64 import b64encode
from werkzeug.security import generate_password_hash
import uuid

hashed_password_admin = generate_password_hash('admintest', method='sha256')
new_admin = UserProfile(publik_id=str(uuid.uuid4()), username='admintest', password=hashed_password_admin, is_admin=True)
db.session.add(new_admin)
db.session.commit()

hashed_password_user = generate_password_hash('usertest', method='sha256')
new_user = UserProfile(publik_id=str(uuid.uuid4()), username='usertest', password=hashed_password_user)
db.session.add(new_user)
db.session.commit()

new_customer_admin = Customer(user_id=new_admin.id, nama="customeradmintest", ukuran_baju="M", email="customeradmintest@email.com")
db.session.add(new_customer_admin)
db.session.commit()

new_customer_user = Customer(user_id=new_user.id, nama="customerusertest", ukuran_baju="M", email="customerusertest@email.com")
db.session.add(new_customer_user)
db.session.commit()

token_admin = app.test_client().get("/login", headers={"Authorization": "Basic {user}".format(user=b64encode(b"admintest:admintest").decode())})
token_user = app.test_client().get("/login", headers={"Authorization": "Basic {user}".format(user=b64encode(b"usertest:usertest").decode())})

class TeesTest(unittest.TestCase):
    # cek apakah aplikasi berjalan dengan baik
    user = UserProfile.query.filter_by(username="admintest").first()
    customer = Customer.query.filter_by(user_id=user.id).first()

    def test_01_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Ensure that the login page loads correctly
    def test_02_token_login_salah(self):
        tester = app.test_client(self)
        response = tester.get("/login", headers={"Authorization": "Basic {user}".format(user=b64encode(b"admin:bukanadmin").decode())})
        self.assertEqual(response.status_code, 401)

    # Ensure that the login page loads correctly
    def test_03_token_login_benar(self):
        tester = app.test_client(self)
        response = tester.get("/login", headers={"Authorization": "Basic {user}".format(user=b64encode(b"admin:admin").decode())})
        self.assertEqual(response.status_code, 200)

    ############ create customer #############################
    def test_04_membuat_customer_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.post("/customer", data=dict(nama="customerbaru", email="customer@email.com", ukuran_baju="M"))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")

    def test_05_membuat_customer_dengan_token(self):
        tester = app.test_client(self)
        response = tester.post("/customer", data=dict(nama="customerbaru", email="customer@email.com", ukuran_baju="M"), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "Customer berhasil dibuat!")

    ################## read customer #########################
    def test_06_menampilkan_semua_customer_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.get("/customer")
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")
    
    def test_07_menampilkan_satu_customer_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.get("/customer/"+str(self.customer.id))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")

    def test_08_menampilkan_semua_customer_dengan_token(self):
        tester = app.test_client(self)
        response = tester.get("/customer", headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.status_code, 200)
    
    def test_09_menampilkan_satu_customer_dengan_token(self):
        tester = app.test_client(self)
        response = tester.get("/customer/"+str(self.customer.id), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.status_code, 200)
    
    def test_10_menampilkan_satu_customer_user_lain(self):
        tester = app.test_client(self)
        response = tester.get("/customer/"+str(self.customer.id), headers={"x-access-token": token_user.json['token']})
        self.assertEqual(response.status_code, 200)
    
    def test_11_menampilkan_satu_customer_yang_tidak_ada_di_database_dengan_token(self):
        tester = app.test_client(self)
        response = tester.get("/customer/id_acak", headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "Customer tidak ditemukan !")

    ################### update customer #######################
    def test_12_mengubah_customer_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.put("/customer/"+str(self.customer.id), data=dict(nama="updatenamacustomer", ukuran_baju="L", email="updatecustomeremail@email.com"))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")
    
    def test_13_mengubah_customer_dengan_token(self):
        tester = app.test_client(self)
        response = tester.put("/customer/"+str(self.customer.id), data=dict(nama="updatenamacustomer", ukuran_baju="L", email="updatecustomeremail@email.com"), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "Customer berhasil terupdate !")
    
    def test_14_mengubah_customer_user_lain(self):
        tester = app.test_client(self)
        response = tester.put("/customer/"+str(self.customer.id), data=dict(nama="updatenamacustomer", ukuran_baju="L", email="updatecustomeremail@email.com"), headers={"x-access-token": token_user.json['token']})
        self.assertEqual(response.json["message"], "Customer tidak ditemukan !")

    def test_15_mengubah_customer_yang_tidak_ada_di_database(self):
        tester = app.test_client(self)
        response = tester.put("/customer/idacak", data=dict(nama="updatenamacustomer", ukuran_baju="L", email="updatecustomeremail@email.com"), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "Customer tidak ditemukan !")

    ################## delete customer ########################
    def test_16_menghapus_customer_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.delete("/customer/"+str(self.customer.id))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")
    
    def test_17_menghapus_customer_user_lain(self):
        tester = app.test_client(self)
        response = tester.delete("/customer/"+str(self.customer.id), headers={"x-access-token": token_user.json['token']})
        self.assertEqual(response.json["message"], "Customer tidak ditemukan !")

    def test_18_menghapus_customer_dengan_token(self):
        tester = app.test_client(self)
        response = tester.delete("/customer/"+str(self.customer.id), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "Customer Berhasil dihapus!")


    def test_19_menghapus_customer_yang_tidak_ada_di_database(self):
        tester = app.test_client(self)
        response = tester.delete("/customer/idacak", headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "Customer tidak ditemukan !")

    ############ create user #############################
    def test_20_membuat_user_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.post("/user", data=dict(username="userbaru", password="passwordbaru"))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")

    def test_21_membuat_user_dengan_token_admin(self):
        tester = app.test_client(self)
        response = tester.post("/user", data=dict(username="userbaru", password="passwordbaru"), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "User Baru Berhasil Dibuat !")

    def test_22_membuat_user_dengan_token_user_biasa(self):
        tester = app.test_client(self)
        response = tester.post("/user", data=dict(username="userbaru", password="passwordbaru"), headers={"x-access-token": token_user.json['token']})
        self.assertEqual(response.json["message"], "Tidak Dapat Menjalankan Fungsi !")

    ################## read user #########################
    def test_23_menampilkan_semua_user_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.get("/user")
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")
    
    def test_24_menampilkan_satu_user_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.get("/user/"+self.user.publik_id)
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")

    def test_25_menampilkan_semua_user_dengan_token(self):
        tester = app.test_client(self)
        response = tester.get("/user", headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.status_code, 200)
    
    def test_26_menampilkan_satu_user_dengan_token(self):
        tester = app.test_client(self)
        response = tester.get("/user/"+self.user.publik_id, headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.status_code, 200)
    
    def test_27_menampilkan_satu_user_yang_tidak_ada_di_database_dengan_token(self):
        tester = app.test_client(self)
        response = tester.get("/user/id_acak", headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "User Tidak ditemukan!")

    ################### update user #######################
    def test_28_mengubah_user_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.put("/user/"+self.user.publik_id, data=dict(nama_lengkap="updateusernamalengkap", alamat="updateuseralamat", email="updateuseremail"))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")
    
    def test_29_mengubah_user_dengan_token_user_biasa(self):
        tester = app.test_client(self)
        response = tester.put("/user/"+self.user.publik_id, data=dict(nama_lengkap="updateusernamalengkap", alamat="updateuseralamat", email="updateuseremail"), headers={"x-access-token": token_user.json['token']})
        self.assertEqual(response.json["message"], "Tidak Dapat Menjalankan Fungsi !")

    def test_30_mengubah_user_dengan_token_admin(self):
        tester = app.test_client(self)
        response = tester.put("/user/"+self.user.publik_id, data=dict(nama_lengkap="updateusernamalengkap", alamat="updateuseralamat", email="updateuseremail"), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "User berhasil terupdate !")

    def test_31_mengubah_user_yang_tidak_ada_di_database(self):
        tester = app.test_client(self)
        response = tester.put("/user/idacak", data=dict(nama_lengkap="updateusernamalengkap", alamat="updateuseralamat", email="updateuseremail"), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "User Tidak ditemukan!")

    ################## delete user ########################
    def test_32_menghapus_user_tanpa_token(self):
        tester = app.test_client(self)
        response = tester.delete("/user/"+str(self.user.publik_id))
        self.assertEqual(response.json["message"], "Token tidak ditemukan !")

    def test_33_menghapus_user_dengan_token_user_biasa(self):
        tester = app.test_client(self)
        response = tester.delete("/user/"+str(self.user.publik_id), headers={"x-access-token": token_user.json['token']})
        self.assertEqual(response.json["message"], "Tidak Dapat Menjalankan Fungsi !")

    def test_34_menghapus_user_yang_tidak_ada_di_database(self):
        tester = app.test_client(self)
        response = tester.delete("/user/idacak", headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "User tidak ditemukan!")

    def test_35_menghapus_user_dengan_token_admin(self):
        tester = app.test_client(self)
        response = tester.delete("/user/"+ str(self.user.publik_id), headers={"x-access-token": token_admin.json['token']})
        self.assertEqual(response.json["message"], "User Berhasil Dihapus!")


    def test_9999999_clear_data(self):
        db.session.delete(UserProfile.query.filter_by(username="usertest").first())
        db.session.delete(UserProfile.query.filter_by(username="userbaru").first())
        db.session.delete(Customer.query.filter_by(nama="customerusertest").first())
        db.session.delete(Customer.query.filter_by(nama="customerbaru").first())
        db.session.commit()

if __name__ == '__main__':
    unittest.main()

    print("wow")
