from flask import Flask,request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_uploads import UploadSet, configure_uploads, IMAGES
from datetime import datetime, timedelta
from functools import wraps
import uuid
import jwt
import os

BASE_DIR =os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

app = Flask(__name__)

app.config['SECRET_KEY']= "ini_secret_key_untuk_JWT"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= False
# app.config["SQLALCHEMY_DATABASE_URI"]= "sqlite:///db.sqlite3"                                 # jika hanya dijalankan di lokal dan menggunakan databace sqlite
# app.config['SQLALCHEMY_DATABASE_URI']= "mysql+pymysql://username:password@host:port/dab_name" # jika database menggunakan mysql
app.config['SQLALCHEMY_DATABASE_URI']= "mysql+pymysql://root:root@db:3306/teestest"             # jika database menggunakan mysql dan berjalan di docker


foto = UploadSet('foto', IMAGES)

#Direktori untuk upload foto profil
app.config['UPLOADED_FOTO_DEST'] = "static/profile_picture"  
app.config['UPLOADED_FOTO_URL'] = "static/profile_picture/"  
configure_uploads(app,foto)

####################################################### deklarasi Model database ############################################
db = SQLAlchemy(app)

class UserProfile(db.Model):
    id              = db.Column(db.Integer, primary_key=True)
    publik_id       = db.Column(db.String(50), unique=True)
    username        = db.Column(db.String(50))
    password        = db.Column(db.String(80))
    email           = db.Column(db.String(80))
    nama_lengkap    = db.Column(db.String(100))
    alamat          = db.Column(db.Text)
    no_hp           = db.Column(db.String(15))
    profile_picture  = db.Column(db.String(225))
    is_admin        = db.Column(db.Boolean)

    date_created    = db.Column(db.DateTime, default=datetime.now)

class Customer(db.Model):
    id              = db.Column(db.Integer, primary_key=True)
    nama            = db.Column(db.String(100))
    email           = db.Column(db.String(80))
    ukuran_baju     = db.Column(db.String(3))
    user_id         = db.Column(db.Integer)

####################################################### deklarasi Model database ############################################

######################################################## initial database model  ############################################

db.create_all()

if UserProfile.query.all() == [] :
    hashed_password = generate_password_hash('admin', method='sha256')
    new_user = UserProfile(publik_id=str(uuid.uuid4()), username='admin', password=hashed_password, is_admin=True)
    db.session.add(new_user)
    db.session.commit()

######################################################## initial database model ############################################



########################################################## JWT AUTHENTICATION #################################################################

@app.route('/login')
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

    user = UserProfile.query.filter_by(username=auth.username).first()

    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

    if check_password_hash(user.password, auth.password):
        token = jwt.encode({'publik_id' : user.publik_id, 'exp' : datetime.utcnow() + timedelta(minutes=30)}, app.config['SECRET_KEY'])

        return jsonify({'token' : token.decode('UTF-8')})

    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message' : 'Token tidak ditemukan !'}), 401

        try: 
            data = jwt.decode(token, app.config['SECRET_KEY'])
            user_saat_ini = UserProfile.query.filter_by(publik_id=data['publik_id']).first()
        except:
            return jsonify({'message' : 'Token tidak valid !'}), 401

        return f(user_saat_ini, *args, **kwargs)

    return decorated
########################################################## END JWT AUTHENTICATION #################################################################


@app.route("/", methods=["GET"])
def home():
    data = {
        "message" : "Simpel API dibangun dengan framework Python Flask dan autentikasi JWT ",
        "aplikasi disarankan" : "postman",
        "GIT"  : "https://gitlab.com/123rosyid123/teestest",
        "docker"    : "https://hub.docker.com/r/123rosyid123/tees_test"
    }
    return jsonify(data)

########################################################### CREATE USER PROFILE #########################################################

@app.route('/user', methods=['POST'])
@token_required
def create_user(user_saat_ini):
    if not user_saat_ini.is_admin:
        return jsonify({'message' : 'Tidak Dapat Menjalankan Fungsi !'})

    data = request.form
    gambar = request.files["photo"] if "photo" in request.files else None
        
    new_user = UserProfile(publik_id=str(uuid.uuid4()), is_admin=False)

    hashed_password = generate_password_hash(data['password'], method='sha256')
    for key in data:
        if key == "password":
            hashed_password = generate_password_hash(data[key], method='sha256')
            setattr(new_user, key, hashed_password)            
        else:
            setattr(new_user, key, data[key])
    if gambar != None:
        new_user.profile_picture = foto.save(gambar) #mengupload foto ke server dan menyimpan nama foto ke db
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message' : 'User Baru Berhasil Dibuat !'})

########################################################### END CREATE USER PROFILE #######################################################

########################################################### READ USER PROFILE #######################################################
@app.route('/user', methods=['GET'])
@token_required
def get_users(user_saat_ini):

    users = UserProfile.query.filter_by(publik_id=user_saat_ini.publik_id) if not user_saat_ini.is_admin else UserProfile.query.all()

    output = []

    for user in users:
        user_data = {}
        user_data['publik_id'] = user.publik_id
        user_data['username'] = user.username
        user_data['nama_lengkap'] = user.nama_lengkap
        user_data['email'] = user.email
        user_data['alamat'] = user.alamat
        user_data['no_hp'] = user.no_hp
        user_data['profile_picture'] = user.profile_picture
        user_data['profile_picture_url'] = request.url_root+foto.url(user.profile_picture) if user.profile_picture != None else None
        user_data['is_admin'] = user.is_admin
        
        output.append(user_data)

    return jsonify({'users' : output})

@app.route('/user/<publik_id>', methods=['GET'])
@token_required
def detail_user(user_saat_ini, publik_id):

    if not user_saat_ini.is_admin and user_saat_ini.publik_id != publik_id:
        return jsonify({'message' : 'Akses ditolak !'})

    user = UserProfile.query.filter_by(publik_id=publik_id).first()

    if not user:
        return jsonify({'message' : 'User Tidak ditemukan!'})

    user_data = {}
    user_data['publik_id'] = user.publik_id
    user_data['username'] = user.username
    user_data['nama_lengkap'] = user.nama_lengkap
    user_data['email'] = user.email
    user_data['alamat'] = user.alamat
    user_data['no_hp'] = user.no_hp
    user_data['profile_picture'] = user.profile_picture
    user_data['password'] = user.password
    user_data['is_admin'] = user.is_admin
    
    return jsonify({'user' : user_data})
########################################################### END READ USER PROFILE #######################################################


########################################################### UPDATE USER PROFILE ###########################################################

@app.route('/user/<publik_id>', methods=['PUT'])
@token_required
def update_user(user_saat_ini, publik_id):

    #jika bukan admin dan id user berbeda dengan parameter maka akses ditolak
    if not user_saat_ini.is_admin and user_saat_ini.publik_id != publik_id:
        return jsonify({'message' : 'Tidak Dapat Menjalankan Fungsi !'})
    
    user = UserProfile.query.filter_by(publik_id=publik_id).first()
    if not user:
        return jsonify({'message' : 'User Tidak ditemukan!'})

    data = request.form
    gambar = request.files["photo"] if "photo" in request.files else None

    for key in data:
        if key == "password":
            hashed_password = generate_password_hash(data[key], method='sha256')
            setattr(user, key, hashed_password)            
        elif key == "is_admin":               
            if not user_saat_ini.is_admin:
                continue
            elif data[key] == "True" or data[key] == "true":
                setattr(user, key, True)
            elif data[key] == "False" or data[key] == "false":
                setattr(user, key, False)
        else:
            setattr(user, key, data[key])
    if gambar != None:
        try:
            os.remove(os.path.join(app.config['UPLOADED_FOTO_DEST'], user.profile_picture))     #delete foto lama
        except:
            pass
        user.profile_picture = foto.save(gambar)                                            #upload foto baru

    db.session.commit()            
    return jsonify({'message' : 'User berhasil terupdate !'})
    
########################################################### END UPDATE USER PROFILE #######################################################

########################################################### DELETE USER PROFILE ###########################################################

@app.route('/user/<publik_id>', methods=['DELETE'])
@token_required
def delete_user(user_saat_ini, publik_id):
    if not user_saat_ini.is_admin:
        return jsonify({'message' : 'Tidak Dapat Menjalankan Fungsi !'}),403

    user = UserProfile.query.filter_by(publik_id=publik_id).first()

    if not user:
        return jsonify({'message' : 'User tidak ditemukan!'})
    
    try:
        os.remove(os.path.join(app.config['UPLOADED_FOTO_DEST'], user.profile_picture))     #delete foto user
    except:
        pass

    db.session.delete(user)
    db.session.commit()

    return jsonify({'message' : 'User Berhasil Dihapus!'})
########################################################### END DELETE USER PROFILE ######################################################


########################################################### READ CUSTOMER ################################################################

@app.route('/customer', methods=['GET'])
@token_required
def get_customers(user_saat_ini):
    customer = Customer.query.filter_by(user_id=user_saat_ini.id).all()

    output = []

    for cust in customer:
        cust_data = {}
        cust_data['id'] = cust.id      #jika tidak diperlukan bisa dicomment
        cust_data['nama'] = cust.nama
        cust_data['email'] = cust.email
        cust_data['ukuran_baju'] = cust.ukuran_baju
        output.append(cust_data)

    return jsonify({'Customer' : output})

@app.route('/customer/<cust_id>', methods=['GET'])
@token_required
def detail_customer(user_saat_ini, cust_id):
    customer = Customer.query.filter_by(id=cust_id, user_id=user_saat_ini.id).first()

    if not customer:
        return jsonify({'message' : 'Customer tidak ditemukan !'})

    customer_data = {}
    customer_data['id'] = customer.id   #jika tidak diperlukan bisa dicomment
    customer_data['nama'] = customer.nama
    customer_data['email'] = customer.email
    customer_data['ukuran_baju'] = customer.ukuran_baju

    return jsonify(customer_data)
########################################################### END READ CUSTOMER ############################################################


########################################################### CREATE CUSTOMER ##############################################################
@app.route('/customer', methods=['POST'])
@token_required
def create_customer(user_saat_ini):
    data = request.form

    customer = Customer(user_id=user_saat_ini.id)
    for key in data:
        setattr(customer,key,data[key])
    db.session.add(customer)
    db.session.commit()

    return jsonify({'message' : "Customer berhasil dibuat!"})
########################################################### END CREATE CUSTOMER ############################################################


########################################################### UPDATE CUSTOMER ################################################################
@app.route('/customer/<cust_id>', methods=['PUT'])
@token_required
def update_customer(user_saat_ini, cust_id):
    customer = Customer.query.filter_by(id=cust_id, user_id=user_saat_ini.id).first()

    if not customer:
        return jsonify({'message' : 'Customer tidak ditemukan !'})
    
    data = request.form    
    for key in data:
        setattr(customer,key,data[key])

    db.session.commit()

    return jsonify({'message' : 'Customer berhasil terupdate !'})
########################################################### END UPDATE CUSTOMER ############################################################


########################################################### DELETE CUSTOMER ################################################################
@app.route('/customer/<cust_id>', methods=['DELETE'])
@token_required
def delete_customer(user_saat_ini, cust_id):
    customer = Customer.query.filter_by(id=cust_id, user_id=user_saat_ini.id).first()

    if not customer:
        return jsonify({'message' : 'Customer tidak ditemukan !'})

    db.session.delete(customer)
    db.session.commit()

    return jsonify({'message' : 'Customer Berhasil dihapus!'})
########################################################### END DELETE CUSTOMER #############################################################


if __name__ == '__main__':
    app.run(debug=1,host="0.0.0.0", port=5000, use_reloader=True)
